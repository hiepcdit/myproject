<!DOCTYPE HTML>
<html>
<head>

    <meta charset="UTF-8"/>
    <title>TEST HTML &AMP; CSS</title>
    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo 'templates/css/style.css' ?>"/>

    <!-- JS -->
    <script src="templates/js/jquery-2.0.3.js"></script>
    <script src="templates/js/back-top.js"></script>
    <script src="templates/js/menu-fade.js"></script>
    <script src="templates/js/load-content.js"></script>    
      
</head>

<body>
<div id="container">
    <div id="header">
        <div id="header-main">
        <div class = "logo">
             <h1><a href="<?php echo 'index.php'; ?>">Continuum</a></h1>
        </div><!-- End .logo -->
        
        <div id = "nav-menu" >
             <ul class="menu">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">About</a>
                    <ul>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Full Width</a></li>
                        <li><a href="#">ShortCodes</a></li>
                    </ul>
                </li>
                <li><a href="#">Contact</a>
                    <ul>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Full dfsfsdf Width</a></li>
                        <li><a href="#">ShortCodes</a></li>
                    </ul>
                </li>
                <li><a href="#">Full Width</a></li>
                <li><a href="#">ShortCodes</a></li>

                <?php if(isset($_SESSION["email"])){ ?>

                <li class="log"><a href="index.php?controller=user&action=view"><?php echo $_SESSION["email"]; ?></a>
                    
                    <ul>
                        <li><a href="index.php?controller=user&action=view">Thông tin cá nhân</a></li>
                        <li><a href="#">Thay đổi mật khẩu</a></li>
                        <li><a href="#">Bài post</a></li>
                        <li><a href="#">Tin nhắn</a></li>
                    </ul>

                    <script>
                    jQuery(document).ready(function($) {
                        $(".log a").css({
                            color: '#0000CC',
                        });

                         $(".logout a").css({
                            color: '#FF3300',
                        });
                    });
                    </script>


                </li>
                <li class="logout" ><a href="index.php?controller=user&action=logout">Logout</a></li>
                <?php } else { ?> 

                <li><a href="index.php?controller=user&action=login">Login</a></li>
                <li><a href="index.php?controller=user&action=register">Register</a></li>

                <?php }  //end if ?>
             </ul>
        </div><!-- End #nav-menu -->
        </div><!-- End #header-main -->

    </div><!-- End #header -->
    
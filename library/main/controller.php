<?php
class Controller{
    
    protected $_arrParam;
    
    //gan thong tin request vao mang _arrParam
    public function __construct(){
        //echo '<br/>' . __METHOD__;
        session_start();
        $this -> _arrParam = $this->getParam();
        
    }
    
    //lay thong tin gui di tu post va get
    public function getParam(){
        
        $get = $_GET;
        $post = $_POST;
        
        if(!isset($get['controller'])){
            $get['controller'] = 'index';
        }
        
         if(!isset($get['action'])){
            $get['action'] = 'index';
        }
        
        $params = array_merge($get,$post);
        
        return $params;
    }
    
    //lien ket voi model
    public function getModel($modelName){
        
        require_once (LIBRARY . '/main/Model.php');
        $modelClass = ucfirst($modelName);
        require_once (APPLICATION . '/models/' . $modelClass . '.php');
        $model = new $modelClass();

        return $model;
        
    }
    
    //tra ve view mong muon
    public function render($view = null, $param = null){
        
        if($view == null){
            $path = APPLICATION . '/views/'
                                . $this -> _arrParam['controller'] . '/'
                                . $this -> _arrParam['action'] . '.php';
        }else{ 
            $path = APPLICATION . '/views/' 
                                . $this -> _arrParam['controller'] . '/' 
                                . $view . '.php';
        }
        
        require_once (LIBRARY . '/main/Template.php');
        $template = new Template();
        $template -> data = $param;
        $template -> create($path);
        }

    public function redirect($controller = null ,$action = null){

        if($controller == null || $action == null ){

            echo "<script type='text/javascript' charset='utf-8'>" 
            . "window.location='index.php'"
            . "</script>" ;

        }else{

            echo "<script type='text/javascript' charset='utf-8'>" 
            . "window.location='index.php?"
            . "controller=". $controller 
            . "&action=". $action
            . "'"
            . "</script>" ;
        }
        
        
    }
}
?>
<?php
class Model{
    private $hostname = DB_HOST;
    private $username = DB_USER;
    private $password = DB_PASSWORD;
    private $dbname   = DB_NAME; 
    
    protected $connect;
    private $flagConnect = false;
    protected $result;
    // protected $affected ;
    
    //Phuong thuc ket noi database
    public function __construct(){
        $conn = new mysqli($this->hostname, $this->username, $this->password, $this->dbname);
        if($conn->connect_error){
            die("Ket noi chua thanh cong:".$conn->connect_error);
        }else{
            $this->connect = $conn;
            $this->flagConnect = true;
        }//end If
    }
    
    //Thuc hien cau lenh sql
    public function execute($sql){
        if($this->flagConnect == true){
            $this->connect->query("SET NAMES 'UTF-8'");
            $this->connect->query("SET CHARACTER SET 'utf-8'");
            $this->result = $this->connect->query($sql);
            // $this->affected = mysqli_affected_rows($this->connect);
            return $this->result;
        }
    }

    //phuong thuc truy xuat du lieu trong bang
    public function fetchAll($table, $id){
        if($this->flagConnect == true){
            $sql = " SELECT * FROM " . $table .
                   " WHERE id = " . $id;
            $this->connect->query("SET NAMES 'UTF-8'");
            $this->connect->query("SET CHARACTER SET 'utf-8'");
            $this->result = $this->connect->query($sql);   
            return $this->result;
        }//end If
    }
    
    //dong ket noi - giai phong bo nho
    public function __destruct(){
        if($this->flagConnect == true){
            if($this->result !=null){
               @$This->connect->free_result($this->result);
            }
            $this->connect->close();
        }
    }
}
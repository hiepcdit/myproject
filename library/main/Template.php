<?php
class Template{
    
    function __construct(){
       // echo __METHOD__;
    }
    
    function __set($name,$value){
        $this->$name = $value;
    }
    
    function __get($name){
        return $this->$name;
    }
    
    function create($path){
        
        include (TEMPLATE . '/header.php');
        include ($path);
        include (TEMPLATE . '/footer.php');
    }
}
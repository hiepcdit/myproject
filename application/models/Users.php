<?php

class Users extends Model{
    
    public function login($data){
       // echo __METHOD__;


        $email = mysqli_real_escape_string($this->connect,strip_tags($data['email']));
        $pass  = mysqli_real_escape_string($this->connect,strip_tags($data['password']));

        $sql   = "SELECT id, email FROM users WHERE `email` = '$email' and `password` = '$pass' limit 1";

            $result = $this->execute($sql);

            $array = array();
            $array['plag'] = FALSE;
            if(mysqli_affected_rows($this->connect) == 1){
                $array= mysqli_fetch_array($result,MYSQL_ASSOC);
                $array['plag']  = TRUE;
            }else{
                $array['plag'] = FALSE;
            }
        return $array;
    }

    public function register($data){

        $email = mysqli_real_escape_string($this->connect,strip_tags($data['email']));
        $pass  = mysqli_real_escape_string($this->connect,strip_tags($data['password']));

        $sql   = " INSERT INTO users(email,password,created_at) VALUES('$email','$pass',now())";
        $this->execute($sql);
        $array = array();
        $array['plag'] = FALSE;
        if(mysqli_affected_rows($this->connect) == 1){
            $array['plag']  = TRUE;
        }
        return $array;
    }

    public function viewUser($id)
    {
        $result = $this->fetchAll('users',$id);

        $sql = " SELECT email, name, DATE_FORMAT(birthday,'%m/%d/%Y') AS birthday
                 FROM users 
                 WHERE id = {$id} ";
        $result = $this->execute($sql);
        $data = mysqli_fetch_array($result,MYSQL_ASSOC);
        return $data;
    }


    public function editUser($id,$array)
    {
        

        //Tách chuỗi ngày tháng thành năm/tháng/ngày 
        if($array['data'] == 'birthday' && !empty( $array['value'])){

            //kiem tra kieu ngay tháng ngay/thang/nam
            $date = $array['value'];
            $format = 'd/m/Y';
            $d = DateTime::createFromFormat($format, $date);
            if(!$d){
                return false;
                exit;
            }

            $tmp = explode('/', $array['value']);

            $birthday = $tmp[2] . '/' . $tmp[1] . '/' .$tmp[0];

            $array['value'] = $birthday;

        }else{

             $array['value'];
        }

        $array['value'] = mysqli_real_escape_string($this->connect,strip_tags($array['value']));

        //Update thông tin
        $sql = " UPDATE users 
                 SET `{$array['data']}` = '{$array["value"]}'
                 WHERE `id` = {$id} ";

        $query = $this->execute($sql);


        if($query){
            return true;
        }else{
            return false;
        }
        
    }
}
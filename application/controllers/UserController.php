<?php
class UserController extends Controller{
    
    public function __construct(){
        parent::__construct();
    }
    
    public function indexAction(){
        //echo __METHOD__;
        $this->render(null);
    }

    //Login acount
    public function loginAction(){

        $postData = $this->_arrParam;
        unset($postData['controller']);
        unset($postData['action']);
        
        if(!empty($postData)){
            //kiem tra email : password
            $error = array();
            //validate
            $validate = $this->getModel('Validates');
            $error   = $validate->validateLogin(array(
                                                    'email'=>$postData['email'],
                                                    'pass' =>$postData['password']
                                                 ));
            if(empty($error)){
                //Không tồn tại error: Xử lý
                $model = $this->getModel('Users');
                $result = $model->login($postData); 

                switch ($result['plag']) {
                    case TRUE:
                        $array= explode('@',$result['email']);
                        //$this->render('success');
                        $_SESSION["email"] = $array[0];
                        $_SESSION['id'] = $result['id'];

                        //chuyen den controller: user action: view
                        $this->redirect('user','view');
                        break;
                    
                    default:
                        $error['fail'] = "Đang nhập chưa thành công!";
                        $this->render(null,$error);
                        break;
                }//end switch


            }else{
                //Tồn tại error:
                $this->render(null,$error);
            }//end if(empty($error))

        }else{
            $this->render(null);
        }//end if(empty($postData))

    }

    //Đăng ký
    public function registerAction(){
       // echo __METHOD__;

        $postData = $this->_arrParam;
        unset($postData['controller']);
        unset($postData['action']);

        if(!empty($postData)){
            //kiem tra email : password cho vao mang $error

            //kiem tra email : password
            $error = array();
            //validate
            $validate = $this->getModel('Validates');
            $error   = $validate->validateRegister(array(
                                                    'email'=>$postData['email'],
                                                    'pass' =>$postData['password'],
                                                    'r_pass' =>$postData['r_password'],
                                                    'captcha' =>$postData['captchar']
                                                 ));
            if(empty($error)){
                //Không tòn tại error: Xử lý
                $model = $this->getModel('Users');
                $result = $model->register($postData); 

                $save = array();
                switch ($result['plag']) {
                    case TRUE:
                        $save['success'] = "Đăng ký thành công!";
                        $this->render(null,$save);
                        break;   
                    default:
                        $save['fail'] = "Đăng ký chưa thành công! <br/> Email: Đã tồn tại!";
                        $this->render(null,$save);
                        break;
                }

            }else{
                //Tồn tại error:
                $this->render(null,$error);
            }//end if(empty($error))

        }else{
            $this->render(null);
        }//end if(empty($postData))
    }

    public function viewAction($id = null){

        $postData = $this->_arrParam;
        unset($postData['controller']);
        unset($postData['action']);
        $id = $_SESSION['id'];
   
        if(empty($postData)){

            if(isset($_SESSION['id'])){

                $model = $this->getModel('Users');
                $result = $model->viewUser($id); 

                $this->render(null,$result);

            }//end if
        }elseif($postData['type'] == 'edit'){
            $model = $this->getModel('Users');
            $edit = $model->editUser($id,$postData); 
            if($edit){
                $this->render(null, "Success !!!!");
            }else{
                $this->render(null, 'Fail !!!!');
            }
        }
    }
    
    public function editAction()
    {

        $postData = $this->_arrParam;
        unset($postData['controller']);
        unset($postData['action']);

        if(isset($_SESSION['id'])){
            $id = $_SESSION['id'];
        }

        $model  = $this->getModel('Users');
        $edit   = $model->editUser($id,$postData); 

        echo json_encode($edit);

    }
     public function logoutAction() {
        // remove all session variables
        session_unset(); 

        // destroy the session 
        session_destroy(); 

        //ve trang chu
        $this->redirect(null);
     }  
}
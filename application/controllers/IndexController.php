<?php
class IndexController extends Controller{
    
    public function __construct(){
        parent::__construct();
    }
    
    public function indexAction(){

        $this ->render(null);
    }
    
    public function viewAction(){
        echo '<br/>'.__METHOD__;
       
    }
    
    public function addAction(){
        echo '<br/>'.__METHOD__;
    }
    
    public function editAction(){
        echo '<br/>'.__METHOD__;
    }
    
    public function deleteAction(){

    }
}
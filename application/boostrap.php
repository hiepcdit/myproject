<?php
class Bootstrap{
    protected $_params;
    
    function __construct(){
        $this->_params = $this->getParam();
    }
    
    function getParam(){
        $get = $_GET;
        $post = $_POST;
        
        if(!isset($get['controller'])){
            $get['controller'] = 'index';
        }
        
         if(!isset($get['action'])){
            $get['action'] = 'index';
        }
        
        $params = array_merge($get,$post);
        return $params;
    }
    
    function run(){
        require_once (CONFIG . '/config.php');
        require_once (LIBRARY . '/main/controller.php');
        $controllerName = ucfirst($this->_params['controller']).'Controller';
        require_once (APPLICATION . '/controllers/'.$controllerName. '.php');
        $controller = new $controllerName();
        $actionName = $this -> _params['action'] . 'Action';
        $controller -> $actionName();   
    }
}

<?php 
	if (isset($this->data)) {
		$data = $this->data;
	} 

?>
<div id="content-wrap">
        <div id="main-content">
            <div class="title-main">
                <h2>Thông tin User
                </h2>
            </div>

            <div id="form">
            <label style ="color: red "for="">
            	<?php 
            		if(isset($error) && !empty($error['success'])){
            			echo '<p style="color: blue"> '.$error['success'] .'</p><br />'; 
            		} elseif (isset($error) && !empty($error['fail'])) {
            			echo '<p style="color: red">'.$error['fail'] .'</p><br />'; 
            		}
            	?>
        	</label>
            
            <form action="index.php?controller=user&action=view&type=edit" method="POST">
                	<label for="email">Email: </label><br>
                	<input type="text" name="email" id ="email" value="<?php if(isset($data)){ echo $data['email'] ;} ?>" placeholder="Nhập Email" disabled/>
                	<!-- <a style = "text-decoration: none" href="javascript:onchange('email')">Thay đổi</a> -->

                	<br>

                	<label for="name">Name: </label><br>
                	<input type="name" name="name" id="name" value="<?php if(isset($data)){ echo $data['name'] ;} ?>" placeholder="Nhập Name" disabled />
					<a style = "text-decoration: none" href="javascript:onchange('name')">Thay đổi</a>
                	<br>

                	<label for="birthday">Ngày sinh: </label><br>
                	<input type="birthday" name="birthday" id="birthday" value="<?php if(isset($data)){ echo $data['birthday'] ;} ?>" placeholder="Nhập Name" disabled />
					<a style = "text-decoration: none" href="javascript:onchange('birthday')">Thay đổi</a>
                	<br>


                	<!-- <input type="submit" id ="submit" name="submit" value="Lưu change" disabled />&nbsp;&nbsp;&nbsp; -->

                	<script>

                	function onchange (key) {
                		//alert(key);
                		console.log(key);
                		$("#submit").removeAttr('disabled');
                		$("#" + key).removeAttr('disabled');
                		$("#" + key).next().removeAttr('href').attr('href', 'javascript:save("'+ key +'")').text('Lưu');
                	}

                	function save(key) {

                		var value = $("#"+key).val();
                		$("#" + key).next().html('...');
                		$.ajax({
                			url: 'index.php?controller=user&action=edit&data=' + key + '&value=' + value,
                			type: 'GET',
                			dataType: 'json'
                		})
                		.done(function(data) {
                			if (data == true) {
                				alert('Thay đổi thành công.');
                			}else{
                				alert('Thay đổi chưa thành công "ngày/tháng/năm"');
                			};
                			$("#" + key).next().html('Lưu');
                		})
                		.fail(function() {
                			console.log("error");
                		})
                		.always(function() {
                			console.log("complete");
                		});
                		
                	}//end function save()
                	
                	</script>
            </form>
            </div>
        </div><!-- End #slides -->
        
    </div><!-- End #content-wrap -->